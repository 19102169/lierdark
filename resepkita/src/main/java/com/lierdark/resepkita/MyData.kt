package com.lierdark.resepkita

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MyData (
    var cover: String,
    var title: String,
    var content: String,
    var author: String,
    var category: String,
    var detail: String,
): Parcelable