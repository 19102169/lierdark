package com.lierdark.resepkita

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [RecyclerViewFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RecyclerViewFragment : Fragment() {
    private lateinit var rv_mydata: RecyclerView
    private val list = ArrayList<MyData>()

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        list.addAll(getListMyDatas())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_recycler_view, container, false)

        rv_mydata = view.findViewById(R.id.rv_mydata)
        rv_mydata.setHasFixedSize(true)
        showRecyclerCardView(view)

        return view
    }

    private fun showRecyclerCardView(view: View) {
        rv_mydata.layoutManager = LinearLayoutManager(view.context)
        val cardViewMyDataAdapter  = CardViewMyDataAdapter(list, view.context)
        rv_mydata.adapter = cardViewMyDataAdapter
    }

    fun getListMyDatas(): ArrayList<MyData> {
        val dataCover = resources.getStringArray(R.array.data_cover)
        val dataTitle = resources.getStringArray(R.array.data_title)
        val dataContent = resources.getStringArray(R.array.data_content)
        val dataAuthor = resources.getStringArray(R.array.data_author)
        val dataCategory = resources.getStringArray(R.array.data_category)
        val dataDetail = resources.getStringArray(R.array.data_detail)

        val listMyData = ArrayList<MyData>()
        for (position in dataTitle.indices) {
            val myData = MyData(
                dataCover[position],
                dataTitle[position],
                dataContent[position],
                dataAuthor[position],
                dataCategory[position],
                dataDetail[position],
            )
            listMyData.add(myData)
        }

        return listMyData
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RecyclerViewFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RecyclerViewFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}