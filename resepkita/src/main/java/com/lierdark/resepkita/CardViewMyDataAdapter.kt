package com.lierdark.resepkita

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.lierdark.resepkita.DetailActivity
import com.lierdark.resepkita.R

class CardViewMyDataAdapter(private val listMyDatas: ArrayList<MyData>, val context: Context):
    RecyclerView.Adapter<CardViewMyDataAdapter.CardViewViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): CardViewViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_cardview, viewGroup, false)
        return CardViewViewHolder(view)
    }

    override fun onBindViewHolder(holder: CardViewViewHolder, position: Int) {
        holder.bind(listMyDatas[position], context)
    }
    override fun getItemCount(): Int = listMyDatas.size

    class CardViewViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imgCover: ImageView
        val tvTitle: TextView
        val tvContent: TextView
        val tvAuthor: TextView
        val tvCategory: TextView

        init {
            imgCover = itemView.findViewById(R.id.img_item_cover)
            tvTitle = itemView.findViewById(R.id.tv_item_title)
            tvContent = itemView.findViewById(R.id.tv_item_content)
            tvAuthor = itemView.findViewById(R.id.tv_item_author)
            tvCategory = itemView.findViewById(R.id.tv_item_category)
        }

        fun bind(myData: MyData, context: Context) {
            val cg = ColorGenerator.MATERIAL
            with(itemView){
                Glide.with(itemView.context)
                    .load(myData.cover)
                    .placeholder(
                        TextDrawable
                            .builder()
                            .buildRect(myData.title.take(2), cg.randomColor)
                    )
                    .apply(RequestOptions().override(350, 550))
                    .into(imgCover)
            }
            tvTitle.text = myData.title
            tvContent.text = myData.content.take(160)
            tvAuthor.text = myData.author
            tvCategory.text = myData.category
            itemView.setOnClickListener {
                Toast.makeText(itemView.context, "Kamu memilih " + myData.title, Toast.LENGTH_SHORT).show()
                val moveWithObjectIntent = Intent(context, DetailActivity::class.java)
                moveWithObjectIntent.putExtra(DetailActivity.EXTRA_MYDATA, myData)
                context.startActivity(moveWithObjectIntent)
            }
        }
    }
}